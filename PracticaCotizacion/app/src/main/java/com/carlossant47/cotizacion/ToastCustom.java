package com.carlossant47.cotizacion;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ToastCustom {

    public static void toastError(Activity context, String message)
    {
        View layout = context.getLayoutInflater().inflate(R.layout.custom_toast_error,
                (ViewGroup) context.findViewById(R.id.parent_view));
        TextView lbMensaje =layout.findViewById(R.id.lbMensaje);
        lbMensaje.setText(message);
        Toast toast =new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static void toastError(Activity context, int message)
    {
        toastError(context, context.getString(message));
    }



}

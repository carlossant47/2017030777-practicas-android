package com.carlossant47.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class CotizacionActivity extends AppCompatActivity {

    private TextView mLbNombre;
    private TextView mLbFolio;
    private Cotizacion cotizacion;
    private RadioGroup rbPlazos;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lbTotal;
    private TextView lbEnganche;
    private EditText txtDescripcion;
    private EditText txtValor;
    private EditText txtPagInicial;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        getSupportActionBar().setTitle("Cotizacion");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //INITIAL GUI ELEMENTS
        mLbNombre = findViewById(R.id.lbNombre);
        mLbFolio = findViewById(R.id.lbFolio);
        rbPlazos = findViewById(R.id.rbgPlazos);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lbTotal = findViewById(R.id.lbTotal);
        lbEnganche = findViewById(R.id.lbEnganche);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValor = findViewById(R.id.txtValor);
        txtPagInicial = findViewById(R.id.txtPagInicial);

        mLbNombre.setText(getString(R.string.cliente) + getIntent().getExtras().getString("nombre"));
        this.cotizacion = (Cotizacion) getIntent().getExtras().getSerializable("cotizacion");
        mLbFolio.setText(getString(R.string.folio) + String.valueOf(cotizacion.getFolio()));
        this.cotizacion.setPlazos(12);
        rbPlazos.setOnCheckedChangeListener(this.rbPlazosAction());
        this.btnCalcular.setOnClickListener(this.btnCalcularAction());
        this.btnLimpiar.setOnClickListener(this.btnLimpiarAction());
        this.btnRegresar.setOnClickListener(this.btnRegresarAction());


    }

    private RadioGroup.OnCheckedChangeListener rbPlazosAction()
    {
        return new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {
                    case R.id.rb12:
                        cotizacion.setPlazos(12);
                        break;
                    case R.id.rb18:
                        cotizacion.setPlazos(18);
                        break;
                    case R.id.rb24:
                        cotizacion.setPlazos(24);
                        break;
                    case R.id.rb36:
                        cotizacion.setPlazos(36);
                        break;
                }

            }
        };
    }

    private View.OnClickListener btnCalcularAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        };
    }

    public void calcular()
    {
        String descripcion = txtDescripcion.getText().toString();
        String pagInicial = txtPagInicial.getText().toString();
        String valor = txtValor.getText().toString();
        if (descripcion.matches("") || descripcion.matches("") ||
                pagInicial.matches("") || rbPlazos.getCheckedRadioButtonId() == -1) {
            ToastCustom.toastError(CotizacionActivity.this, R.string.campovacio);
        }


        else
        {
            cotizacion.setDescripcion(descripcion);
            cotizacion.setPorEnganche(Float.parseFloat(pagInicial));
            cotizacion.setValorAuto(Float.parseFloat(valor));

            lbEnganche.setText(getString(R.string.enganche)  + " " +
                    dineroFormato(cotizacion.calcularEnganche()));
            lbTotal.setText(getString(R.string.totalPagar) + " " +
                    dineroFormato(cotizacion.calcularPagMensual()));

        }
    }

    private View.OnClickListener btnRegresarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    private View.OnClickListener btnLimpiarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtValor.setText("");
                txtPagInicial.setText("");
                txtDescripcion.setText("");
                lbTotal.setText(R.string.totalPagar);
                lbEnganche.setText(R.string.enganche);
                rbPlazos.check(R.id.rb12);
            }
        };
    }
    private String dineroFormato(float valor)
    {
        return NumberFormat.getCurrencyInstance(new Locale("es", "MX")).format(valor);
    }




}

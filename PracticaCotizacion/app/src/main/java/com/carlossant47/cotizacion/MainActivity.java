package com.carlossant47.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private Button btnIngresar;
    private Button btnSalir;
    private Cotizacion cotizacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        txtNombre = findViewById(R.id.txtNombre);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(this.btnSalirAction());
        btnIngresar.setOnClickListener(this.btnIngresarAction());


        cotizacion = new Cotizacion();
    }


    private View.OnClickListener btnSalirAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    private View.OnClickListener btnIngresarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(!nombre.matches(""))
                {
                    Intent intent = new Intent(MainActivity.this, CotizacionActivity.class);
                    intent.putExtra("nombre", nombre);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("cotizacion", cotizacion);

                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                else
                    ToastCustom.toastError(MainActivity.this, R.string.campovacio);

            }
        };
    }
}

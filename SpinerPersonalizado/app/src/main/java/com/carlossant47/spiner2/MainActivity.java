package com.carlossant47.spiner2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Spinner mSpCategoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSpCategoria = findViewById(R.id.spCategoria);
        ArrayList<Categoria> list = new ArrayList<>();
        list.add(new Categoria(getString(R.string.itemFrappses),
                getString(R.string.msgFrapsses), R.drawable.categorias));

        list.add(new Categoria(getString(R.string.itemAgradecimiento),
                getString(R.string.msgAgradecimiento), R.drawable.agradecimiento));

        list.add(new Categoria(getString(R.string.itemAmor),
                getString(R.string.msgAmor), R.drawable.amor));

        list.add(new Categoria(getString(R.string.itemNewyear),
                getString(R.string.msgNewYear), R.drawable.nuevo));

        list.add(new Categoria(getString(R.string.itemCanciones),
                getString(R.string.msgCanciones), R.drawable.canciones));
        final CategoriaAdapter adapter = new CategoriaAdapter(this, R.layout.spiner_item_layout, R.id.lbCategoria, list);
        mSpCategoria.setAdapter(adapter);
        mSpCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String categoria = ((Categoria) parent.getItemAtPosition(position)).getCategoria();
                Toast.makeText(adapter.getContext(),
                        getString(R.string.msgSeleccionado) + " " + categoria,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }
}

package com.carlossant47.spiner2;

public class Categoria {
    private String categoria;
    private String descripcion;
    private Integer imageId;

    public Categoria(String categoria, String descripcion, Integer imageId) {
        this.categoria = categoria;
        this.descripcion = descripcion;
        this.imageId = imageId;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }
}

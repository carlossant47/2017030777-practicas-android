package com.carlossant47.divisas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {


    private EditText mTxtCantidad;
    private Spinner mSpDivisas;
    private TextView mLbDivisa;
    private TextView mLbTotal;
    private Button mBtnCalcular;
    private Button mBtnLimpiar;
    private Button mBtnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTxtCantidad = findViewById(R.id.txtCantidad);
        mSpDivisas = findViewById(R.id.spDivisas);
        mLbDivisa = findViewById(R.id.lbDivisa);
        mLbTotal = findViewById(R.id.lbTotal);
        mBtnCalcular = findViewById(R.id.btnCalcular);
        mBtnLimpiar = findViewById(R.id.btnLimpiar);
        mBtnSalir = findViewById(R.id.btnSalir);
        ArrayAdapter<String> divisasAdapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.divisas));
        mSpDivisas.setAdapter(divisasAdapter);
        mSpDivisas.setOnItemSelectedListener(this.spDivisasAction());
        mBtnCalcular.setOnClickListener(this.btnCalcularAction());
        mBtnLimpiar.setOnClickListener(this.btnLimpiarAction());
        mBtnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private View.OnClickListener btnLimpiarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLbTotal.setText("");
                mTxtCantidad.setText("");
            }
        };
    }

    private float convertir(int divisa)
    {
        float total = 0.0f;
        float pesos = Float.parseFloat(mTxtCantidad.getText().toString());

        switch (divisa){
            case 0:
                total = pesos / 18.66f;
                break;
            case 1:
                total = pesos / 20.70f;
                break;
            case 2:
                total = pesos / 14.28f;
                break;
            case 3:
                total = pesos / 24.29f;
                break;
        }
        return total;
    }
    private AdapterView.OnItemSelectedListener spDivisasAction() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!mTxtCantidad.getText().toString().matches(""))
                {
                    float total = convertir(position);
                    DecimalFormat df = new DecimalFormat("#.##");
                    mLbTotal.setText("$ " + df.format(total));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private View.OnClickListener btnCalcularAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mTxtCantidad.getText().toString().matches(""))
                {
                    float total = convertir(mSpDivisas.getSelectedItemPosition());
                    DecimalFormat df = new DecimalFormat("#.##");
                    mLbTotal.setText("$ " + df.format(total));

                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.errorCantidad, Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private void nose()
    {

    }





}

package com.carlossant47.practica1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnHola;
    private EditText txtNombre;
    private TextView mLbMensaje;
    private Button mBtnLimpiar;
    private Button mBtnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnHola = findViewById(R.id.btnHola);
        txtNombre = findViewById(R.id.txtNombre);

        mLbMensaje = findViewById(R.id.lbMensaje);
        mBtnLimpiar = findViewById(R.id.btnLimpiar);
        mBtnSalir = findViewById(R.id.btnSalir);
        btnHola.setOnClickListener(this.btnHolaAction());
        mBtnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNombre.setText("");
                mLbMensaje.setText("");

            }
        });
        mBtnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private View.OnClickListener btnHolaAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(!nombre.matches(""))
                    mLbMensaje.setText("Hola " + nombre + " como estas");
                else
                    Toast.makeText(getApplicationContext(), "Por favor introduce tu nombre", Toast.LENGTH_LONG).show();
            }
        };
    }


}

package com.carlossant47.convertidor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

public class ImcActivity extends AppCompatActivity {

    String nombre;
    private EditText txtMetros;
    private EditText txtCentimetros;
    private EditText txtPeso;
    private TextView lbResultado;
    private AppCompatButton btnCalcular;
    private AppCompatButton btnLimpiar;
    private AppCompatButton btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        getSupportActionBar().hide();//HIDE TOOLBAR

        nombre = getIntent().getStringExtra("nombre");

        ((TextView) findViewById(R.id.lbNombre)).setText(String.format("Hola %s", nombre));

        txtMetros = findViewById(R.id.txtMetros);
        txtCentimetros = findViewById(R.id.txtCentimetros);

        txtPeso = findViewById(R.id.txtPeso);
        lbResultado = findViewById(R.id.lbResultado);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        this.btnCalcular.setOnClickListener(this.btnCalcularAction());
        this.btnLimpiar.setOnClickListener(this.btnLimpiarAction());
        this.btnSalir.setOnClickListener(this.btnSalirAction());
    }

    private View.OnClickListener btnCalcularAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!txtMetros.getText().toString().matches("") &&
                        !txtPeso.getText().toString().matches("") &&
                        !txtCentimetros.getText().toString().matches(""))
                {
                    float total;
                    float altura = Float.parseFloat(
                            String.format("%s.%s",
                                    txtMetros.getText().toString(),
                                    txtCentimetros.getText().toString()));
                    //float altura = Float.parseFloat(txtAltura.getText().toString());
                    if(altura >= 10)
                    {
                        altura = altura / 100;
                    }
                    float peso = Float.parseFloat(txtPeso.getText().toString());
                    total = peso / (altura * altura);
                    Log.e("Error", String.valueOf(total));
                    DecimalFormat format = new DecimalFormat("#.##");
                    lbResultado.setText(String.format("%s %s %s", getString(R.string.imc),
                            format.format(total), getString(R.string.imcmedida)));
                }
                else
                {
                    ToastCustom.toastError(ImcActivity.this,
                            "No puedes dejar los campos vacios");
                }

            }
        };
    }

    private View.OnClickListener btnSalirAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    private View.OnClickListener btnLimpiarAction(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMetros.setText("");
                txtCentimetros.setText("");
                txtPeso.setText("");
            }
        };
    }
}

package com.carlossant47.convertidor;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

public class ToastCustom {

    public static void toastSuccess(Activity activity, String message)
    {

        View view = activity.getLayoutInflater().inflate(R.layout.custom_toast_layout,
                (ViewGroup) activity.findViewById(R.id.parent_view));
        ((TextView) view.findViewById(R.id.lbMensaje)).setText(message);
        ((ImageView) view.findViewById(R.id.imgIcon)).setImageResource(R.drawable.ic_xbox);
        ((CardView)view.findViewById(R.id.parent_view)).
                setCardBackgroundColor(activity.getResources().getColor(R.color.green_700));
        Toast toast = new Toast(activity);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(view);
        toast.show();
    }

    public static void toastError(Activity activity, String message)
    {

        View view = activity.getLayoutInflater().inflate(R.layout.custom_toast_layout,
                (ViewGroup) activity.findViewById(R.id.parent_view));
        ((TextView) view.findViewById(R.id.lbMensaje)).setText(message);
        ((CardView)view.findViewById(R.id.parent_view)).
                setCardBackgroundColor(activity.getResources().getColor(R.color.red_800));
        Toast toast = new Toast(activity);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(view);
        toast.show();
    }
}

package com.carlossant47.convertidor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    private AppCompatButton btnSalir;
    private AppCompatButton btnConvertidor;
    private AppCompatButton btnImc;
    private EditText txtNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.btnSalir = findViewById(R.id.btnSalir);
        this.btnConvertidor = findViewById(R.id.btnConvertidor);
        this.btnImc = findViewById(R.id.btnImc);
        this.txtNombre = findViewById(R.id.txtNombre);
        this.btnImc.setOnClickListener(this.btnImcAction());
        this.btnConvertidor.setOnClickListener(this.btnConvertidorAction());
        this.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private View.OnClickListener btnConvertidorAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(!nombre.matches(""))
                {
                    Intent intent = new Intent(MainActivity.this,
                            ConvertidorActivity.class);
                    intent.putExtra("nombre", nombre);
                    startActivity(intent);
                }
                else
                    ToastCustom.toastError(MainActivity.this, "No puedes dejar los campos vacios");
            }
        };
    }

    private View.OnClickListener btnImcAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(!nombre.matches(""))
                {
                    Intent intent = new Intent(MainActivity.this,
                            ImcActivity.class);
                    intent.putExtra("nombre", nombre);
                    startActivity(intent);
                }
                else
                    ToastCustom.toastError(MainActivity.this,
                            "No puedes dejar los campos vacios");
            }
        };
    }


}

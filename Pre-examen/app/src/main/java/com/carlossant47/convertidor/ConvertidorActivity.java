package com.carlossant47.convertidor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

public class ConvertidorActivity extends AppCompatActivity {

    private RadioGroup rbgConvertir;
    private TextView lbResultado;
    private AppCompatButton btnCalcular;
    private AppCompatButton btnLimpiar;
    private AppCompatButton btnSalir;
    private EditText txtTemperatura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_convertidor);
        String nombre = getIntent().getStringExtra("nombre");
        ((TextView) findViewById(R.id.lbNombre)).setText(String.format("Hola %s", nombre));
        rbgConvertir = findViewById(R.id.rbgConvertir);
        lbResultado = findViewById(R.id.lbResultado);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        txtTemperatura = findViewById(R.id.txtTemperatura);
        this.btnCalcular.setOnClickListener(this.btnCalcularAction());
        this.btnSalir.setOnClickListener(this.btnSalirAction());
        this.btnLimpiar.setOnClickListener(this.btnLimpiarAction());
    }

    private View.OnClickListener btnCalcularAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float total = 0.0f;
                if(!txtTemperatura.getText().toString().matches(""))
                {
                    float temperatura = Float.parseFloat(txtTemperatura.getText().toString());
                    char tipo = 'C';
                    switch (rbgConvertir.getCheckedRadioButtonId())
                    {
                        case R.id.rbCel:
                            total = (9 * temperatura / 5) + 32;
                            tipo = 'F';
                            break;
                        case R.id.rbFat:
                            total = (temperatura - 32) * 5/9;
                            break;
                    }
                    Log.e("Error", String.valueOf(total));
                    DecimalFormat format = new DecimalFormat("#.##");
                    lbResultado.setText(String.format("%s %s %s", getString(R.string.resultado_es),
                            format.format(total), tipo));
                }
                else
                {
                    ToastCustom.toastError(ConvertidorActivity.this,
                            "No puedes dejar los campos vacios");
                }

            }
        };
    }

    private View.OnClickListener btnSalirAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    private View.OnClickListener btnLimpiarAction(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtTemperatura.setText("");
            }
        };
    }
}

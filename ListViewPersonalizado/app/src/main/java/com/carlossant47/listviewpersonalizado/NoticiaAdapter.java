package com.carlossant47.listviewpersonalizado;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class NoticiaAdapter extends ArrayAdapter<Noticia> implements Filterable {
    private int id;
    private Activity context;
    private ArrayList<Noticia> list;
    private LayoutInflater inflater;
    private NoticiaFilther filter;
    private ArrayList<Noticia> filterList;
    //TIENES QUE CREAR OTRA LISTA DE TU OBJETO

    public NoticiaAdapter(Activity context, int groupId, int id, ArrayList<Noticia> list)
    {
        super(context, id, list);
        this.list = list;
        this.filterList = list;
        //ASIGNALE LOS MISMOS OBJETOS A ESA LISTA DE FILTRO
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.id = groupId;
    }

    public View getView(int position, View contentView, ViewGroup parent)
    {
        View itemView = inflater.inflate(this.id, parent, false);
        TextView lbTitulo = itemView.findViewById(R.id.lbTitle);
        TextView lbSubtitle = itemView.findViewById(R.id.lbSubtitle);
        TextView lbDate = itemView.findViewById(R.id.lbDate);
        ImageView ivImagen = itemView.findViewById(R.id.ivImagen);

        lbTitulo.setText(this.list.get(position).getTitulo());
        lbSubtitle.setText(this.list.get(position).getSubtitulo());
        lbDate.setText(this.list.get(position).getFecha());
        ivImagen.setImageResource(this.list.get(position).getImagen());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return getView(position, convertView, parent);
    }

    //LOS METODOS GECOUNT, GET ITEM, IDTEMID Y GEFILTER SON OBLIGATPROPS
    @Override
    public int getCount() {
        return list.size();
        //Retorna cuantos elementos hay en la lista, la lista original
    }

    @Override
    public Noticia getItem(int pos) {
        return list.get(pos);

    }

    @Override
    public long getItemId(int pos) {
        return list.indexOf(getItem(pos));
    }

    @Override
    public Filter getFilter() {
        if(this.filter == null)
        {
            filter = new NoticiaFilther();
        }
        //Asigna la clase para realizar la accion de busqueda y fitro
        return filter;
    }

    //creacion de la clase fitro pueden ponerle el nombre que gusten
    private class NoticiaFilther extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results=new FilterResults();
            if(constraint != null && constraint.length() > 0)
            {
                constraint = constraint.toString().toUpperCase();
                ArrayList<Noticia> filter = new ArrayList<>();
                for(int x = 0; x < filterList.size(); x++)
                {
                    //en este if compararan que concidencia tienen lo que escribieron
                    if(filterList.get(x).getTitulo().toUpperCase().contains(constraint))
                    {

                        Noticia not = filterList.get(x);
                        filter.add(not);
                    }
                }
                results.count = filter.size();
                results.values = filter;
            }
            else
            {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list  = (ArrayList<Noticia>) results.values;
            notifyDataSetChanged();
            //AQUI NOSE QUE HACE no porgunten solo pongan lo que les pide
            //el list es el array original
        }
    }



}

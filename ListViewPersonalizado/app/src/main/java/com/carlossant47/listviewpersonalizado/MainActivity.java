package com.carlossant47.listviewpersonalizado;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listCategoria;
    private NoticiaAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initToolbar();
        ArrayList<Noticia> list = new ArrayList<>();
        listCategoria = findViewById(R.id.listCategoria);
        String[] titulos = getResources().getStringArray(R.array.titulosNoticias);


        list.add(new Noticia(titulos[0], "Historia", R.drawable.image1));
        list.add(new Noticia(titulos[1], "Historia", R.drawable.image2));
        list.add(new Noticia(titulos[2], "Historia", R.drawable.image3));
        list.add(new Noticia(titulos[3], "Historia", R.drawable.image4));
        list.add(new Noticia(titulos[4], "Historia", R.drawable.image5));
        list.add(new Noticia(titulos[5], "Historia", R.drawable.image6));
        list.add(new Noticia(titulos[6], "Historia", R.drawable.image7));
        list.add(new Noticia(titulos[7], "Historia", R.drawable.image8));
        list.add(new Noticia(titulos[8], "Historia", R.drawable.image9));
        adapter = new NoticiaAdapter(MainActivity.this,
                R.layout.item_news_image, R.id.lbTitle, list);
        listCategoria.setAdapter(adapter);
        listCategoria.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("MSG", "CLICK");
            }
        });

    }

    private void initToolbar()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Noticias");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu paramMenu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, paramMenu);
        MenuItem item = paramMenu.findItem(R.id.btnSearch);
        SearchView search = (SearchView) item.getActionView();
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(paramMenu);
    }

    
}

package com.carlossant47.listviewpersonalizado;

import java.util.GregorianCalendar;

public class Noticia {

    private String titulo;
    private String fecha;
    private String subtitulo;
    private Integer imagen;

    public Noticia(String titulo, String subtitulo, Integer imagen) {
        this.setTitulo(titulo);
        this.setFecha("12 de Noviembre del 2019");
        this.setSubtitulo(subtitulo);
        this.setImagen(imagen);
    }

    public Noticia()
    {

    }




    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }

    public Integer getImagen() {
        return imagen;
    }

    public void setImagen(Integer imagen) {
        this.imagen = imagen;
    }

    private String randomDate()
    {
        GregorianCalendar calendar = new GregorianCalendar();
        int year = this.randBetween(1986, 2019);
        calendar.set(calendar.YEAR, year);
        int dayOfYear = randBetween(1, calendar.getActualMaximum(calendar.DAY_OF_YEAR));
        calendar.set(calendar.DAY_OF_YEAR, dayOfYear);
        return String.format("%i, %i %i", calendar.get(calendar.DAY_OF_WEEK),
                calendar.get(calendar.DAY_OF_MONTH), calendar.get(calendar.YEAR));
    }
    public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }


}

package com.carlossant47.listviewpersonalizado;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ToastCustom {

    public static void toastImage(Activity activity, Categoria categoria)
    {
        View view = activity.getLayoutInflater().inflate(R.layout.toast_image_layout,
                (ViewGroup) activity.findViewById(R.id.parent_view));
        ((TextView) view.findViewById(R.id.lbCategoria)).setText(categoria.getCategoria());
        ((TextView) view.findViewById(R.id.lbDescripcion)).setText(categoria.getDescripcion());
        ((ImageView) view.findViewById(R.id.imgSelect)).setImageResource(categoria.getImageId());
        Toast tost = new Toast(activity);
        tost.setDuration(Toast.LENGTH_SHORT);
        tost.setView(view);
        tost.show();
    }
}

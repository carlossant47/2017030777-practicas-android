package com.carlossant47.listviewpersonalizado;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CategoriaAdapter extends ArrayAdapter<Categoria> {
    int id;
    Activity context;
    ArrayList<Categoria> list;
    LayoutInflater inflater;

    public CategoriaAdapter(Activity context, int groupId, int id, ArrayList<Categoria> list)
    {
        super(context, id, list);
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.id = groupId;
    }

    public View getView(int position, View contentView, ViewGroup parent)
    {
        View itemView = inflater.inflate(this.id, parent, false);
        ImageView imgCategoria = itemView.findViewById(R.id.imgSelect);
        TextView lbCategoria = itemView.findViewById(R.id.lbCategoria);
        TextView lbDescripcion = itemView.findViewById(R.id.lbDescripcion);
        imgCategoria.setImageResource(list.get(position).getImageId());
        lbCategoria.setText(list.get(position).getCategoria());
        lbDescripcion.setText(list.get(position).getDescripcion());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return getView(position, convertView, parent);
    }





}
